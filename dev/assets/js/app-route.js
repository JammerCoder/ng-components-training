(function(){
	'use strict';

	var app = angular.module('app');

	app.config(function($routeProvider){
		$routeProvider
			.when('/list',{template: "<product-list></product-list>"})
			.when('/about',{template: "<about-page></about-page>"})
			.otherwise({redirectTo: "/list"});
	});
}());