(function(){
	'use strict';

	var app = angular.module('app');

	app.component('itemRating',{
		templateUrl: "dev/assets/views/inventory.products/components/item-rating.html",
		controllerAs: "model",
		bindings: {
			value: "<"
		},
		transclude: true,
		controller: controller
	});

	function controller(){

		var model = this;

		model.$onInit = function(){
			model.stars = new Array(model.value);	
		}

		model.$onChanges = function(){
			model.stars = new Array(model.value);	
		}
	}
}());