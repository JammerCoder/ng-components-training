(function () {
    'use strict';

    var app = angular.module("app");

    app.component("productList", {
        //        template: "<h1>Product List</h1>",        
        templateUrl: "dev/assets/views/inventory.products/product-list.html",
        controllerAs: "model",
        controller: ["$http", controller]
    });

    function controller($http) {
        var model = this;
        model.productList = [];
        model.templateTitle = "Product List";

        model.$onInit = function () {
            getProducts($http).then(function (response) {
                model.productList = response.data;
            });
        }

        model.upRating = function(item){
            if(item.rating < 5){
                item.rating += 1;
            }
        }
        model.downRating = function(item){
            if(item.rating > 1){
                item.rating -= 1;
            }
        }
    }

    function getProducts($http) {
        return $http.get("dev/json/products-details.json")
            .then(function (response) {
                return response;
            });
    }
}());
